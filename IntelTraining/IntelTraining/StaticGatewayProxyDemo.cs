﻿using System;
using NUnit.Framework;

namespace IntelTraining
{
	[TestFixture]
	public class StaticGatewayProxyDemo
	{
		[Ignore]
		[Test]
		public void wrong_way_of_time()
		{
			var data = new SomeData();
			Assert.LessOrEqual(data.Date, DateTime.Now);
			Assert.Fail("this is wrong");
		}

		[Test]
		public void system_time_demo()
		{
			var dt = DateTime.Now;
			SystemTime._replaceCurrentTimeLogic(() => dt);

			var data = new BetterData();
			Assert.AreEqual(dt, data.Date);

			SystemTime._revertToDefaultLogic();
		}

		class SomeData
		{
			public SomeData()
			{
				Date = DateTime.Now;
			}

			public DateTime Date { get; set; }
		}

		class BetterData
		{
			public BetterData()
			{
				Date = SystemTime.Current;
			}

			public DateTime Date { get; set; }
		}
	}
}