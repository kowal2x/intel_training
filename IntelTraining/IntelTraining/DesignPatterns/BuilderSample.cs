﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace IntelTraining.DesignPatterns
{
	[TestFixture]
	public class BuilderSample
	{
		[Test]
		public void builder_works()
		{
			var builder = new RunExternalProgramBuilder();
			builder.AssignSomeSwitch("sw");
			builder.AddRegionParam("region1");
			builder.AddRegionParam("region2");

			var facade = builder.Build();
			Assert.IsInstanceOf<IRunExternalProgramFacade>(facade);
			Assert.Throws<NotImplementedException>(() => facade.Run());
		}
		[Test]
		public void fluent_builder_works()
		{
			var facade = new RunExternalProgramBuilderFluent()
				.AssignSomeSwitch("sw")
				.AddRegionParam("region1")
				.AddRegionParam("region2").Build();

			Assert.IsInstanceOf<IRunExternalProgramFacade>(facade);
			Assert.Throws<NotImplementedException>(() => facade.Run());
		}

		class RunExternalProgramBuilder
		{
			private readonly List<string> _regionParams = new List<string>();
			private string _someSwitch;
			private bool _isSwitchAssigned;

			public void AddRegionParam(string region)
			{
				_regionParams.Add(region);
			}

			public void AssignSomeSwitch(string @switch)
			{
				_someSwitch = @switch;
				_isSwitchAssigned = true;
			}

			public IRunExternalProgramFacade Build()
			{
				if (_isSwitchAssigned == false)
					throw new ArgumentException("switch not assigned");

				return new RunExternalProgramFacade(_regionParams, _someSwitch);
			}
		}

		class RunExternalProgramBuilderFluent
		{
			private readonly List<string> _regionParams = new List<string>();
			private string _someSwitch;
			private bool _isSwitchAssigned;

			public RunExternalProgramBuilderFluent AddRegionParam(string region)
			{
				_regionParams.Add(region);
				return this;
			}

			public RunExternalProgramBuilderFluent AssignSomeSwitch(string @switch)
			{
				_someSwitch = @switch;
				_isSwitchAssigned = true;
				return this;
			}

			public IRunExternalProgramFacade Build()
			{
				if (_isSwitchAssigned == false)
					throw new ArgumentException("switch not assigned");

				return new RunExternalProgramFacade(_regionParams, _someSwitch);
			}
		}

		class OutputData
		{
			public string Path { get; set; }
		}
		interface IRunExternalProgramFacade
		{
			OutputData Run();
		}

		private class RunExternalProgramFacade : IRunExternalProgramFacade
		{
			public RunExternalProgramFacade(
				IEnumerable<string> regionParams, string someSwitch)
			{

			}

			public OutputData Run()
			{
				throw new System.NotImplementedException();
			}
		}
	}
}