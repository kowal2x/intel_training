﻿using System.Collections;
using System.Linq;
using NSubstitute;
using NUnit.Framework;

namespace IntelTraining.ExternalProcessorCase
{
	[TestFixture]
	public class ExternalProcessorRecognition
	{
		[Test]
		public void recognition()
		{
			var data = ExternalService.ExternalService.GetUrl("http://www.wp.pl");
			Assert.NotNull(data);
		}

		[Test]
		public void processor_works()
		{
			var data = ExternalService.ExternalService.GetUrl("http://www.wp.pl");
			var processor = new WebPageLinkProcessor(data);
			var ret = processor.GetUrls();

			Assert.NotNull(ret);
			Assert.Greater(ret.Count(), 0);
		}

		[Test]
		public void processor_works_better_version()
		{
			//IExternalService svc = new ExternalServiceImpl();
			var svc = Substitute.For<IExternalService>();
			svc.GetData(Arg.Any<string>()).Returns("<a href=\"http://www.pajacyk.pl\">srg</a><a href=\"http://www.pajacyk2.pl\">ert</a>");

			var data = svc.GetData("http://www.wp.pl");
			var processor = new WebPageLinkProcessor(data);
			var ret = processor.GetUrls();

			Assert.NotNull(ret);
			Assert.AreEqual(2, ret.Count());
		}
	}
}