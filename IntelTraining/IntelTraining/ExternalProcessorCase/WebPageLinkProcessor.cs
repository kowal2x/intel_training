﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IntelTraining.ExternalProcessorCase
{
	public class WebPageLinkProcessor
	{
		private readonly string _content;

		public WebPageLinkProcessor(string content)
		{
			_content = content;
		}

		public IEnumerable<string> GetUrls()
		{
			var lstRegex = new List<string>();
			var regex = new Regex("(<a.*?>.*?</a>)", RegexOptions.Singleline);
			var innerRegex = new Regex(@"href=\""(.*?)\""", RegexOptions.Singleline);
			var matches = regex.Matches(_content);
			foreach (Match match in matches)
			{
				string value = match.Groups[1].Value;
				var innerMatch = innerRegex.Match(value);
				if (innerMatch.Success)
				{
					string link = innerMatch.Groups[1].Value;
					lstRegex.Add(link);
				}
			}

			return lstRegex;
		}
	}
}