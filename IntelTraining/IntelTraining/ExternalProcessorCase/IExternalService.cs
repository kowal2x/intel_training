﻿namespace IntelTraining.ExternalProcessorCase
{
	public interface IExternalService
	{
		string GetData(string uri);
	}
}