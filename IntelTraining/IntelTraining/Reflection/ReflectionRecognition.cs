﻿using System;
using System.Linq;
using System.Reflection;
using ForReflection;
using NUnit.Framework;
using ReflectionMagic;

namespace IntelTraining.Reflection
{
	[TestFixture]
	public class ReflectionRecognition
	{
		[Test]
		public void reflection_basic()
		{
			object obj = new SomeClassForReflection();

			//var methodInfo = obj.GetType().GetMethod("DoSmth");
			//var ret = (int)methodInfo.Invoke(obj, new object[0]);
			//Assert.AreEqual(1, ret);

			var methodInfo = obj.GetType().GetMethod("DoSmth", new Type[0]);
			var ret = (int)methodInfo.Invoke(obj, new object[0]);
			Assert.AreEqual(1, ret);

			if (obj.GetType().GetMethods().Any(e => e.Name == "DoSmth"))
			{
				var methodInfo2 = obj.GetType().GetMethod("DoSmth", new Type[] { typeof(int) });
				var ret2 = (int)methodInfo2.Invoke(obj, new object[] { 1 });
				Assert.AreEqual(2, ret2);
			}
		}

		[Test]
		public void reflection_dynamic()
		{
			object obj = new SomeClassForReflection();

			dynamic dyn = obj;
			var ret1 = (int)dyn.DoSmth();
			var ret2 = (int)dyn.DoSmth(1);

			Assert.AreEqual(1, ret1);
			Assert.AreEqual(2, ret2);
		}

		[Test]
		public void from_another_assembly()
		{
			Init.Initialize();

			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(e => e.GetTypes())
				.Where(e => e.GetInterfaces().Any(f => f == typeof(IReflectionSmth)))
				.ToList();

			Assert.AreEqual(1, types.Count);

			var theType = types.Single();
			var obj = (IReflectionSmth)Activator.CreateInstance(theType);

			Assert.AreEqual(666, obj.DoSmth());
		}

		[Test]
		public void from_another_assembly_generic()
		{
			Init.Initialize();

			//typeof(IReflectionSmthGeneric<>).MakeGenericType(typeof(int))

			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(e => e.GetTypes())
				.Where(e => e.GetInterfaces()
					.Any(f => f.IsGenericType && f.IsInterface &&
						f.GetGenericTypeDefinition() == typeof(IReflectionSmthGeneric<>)))
				.ToList();

			Assert.AreEqual(1, types.Count);

			var theType = types.Single();
			var obj = Activator.CreateInstance(theType);

			var ret = (int)obj.AsDynamic().DoSmth();

			Assert.AreEqual(666, ret);
		}

		class SomeClassForReflection
		{
			public int DoSmth()
			{
				return 1;
			}

			public int DoSmth(int i)
			{
				return i + 1;
			}
		}
	}
}