﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;
using IntelTraining.Serialization.Helpers;
using NUnit.Framework;

namespace IntelTraining.Serialization
{
	[TestFixture]
	public class SerializationRecognition
	{
		[Test]
		public void can_de_serialize_abstracts()
		{
			var farm = new AnimalFarm();
			farm.Animals.Add(new Cat() { Weight = 10, FurLength = 10 });
			farm.Animals.Add(new Fish() { Weight = 1, ScalesCount = 1 });

			string serializedStr = null;
			Assert.DoesNotThrow(
				() => serializedStr = SerializationHelper.ToString(farm));
			Assert.NotNull(serializedStr);
			Assert.IsFalse(string.IsNullOrWhiteSpace(serializedStr));

			AnimalFarm deserializedFarm = null;
			Assert.DoesNotThrow(
				() => deserializedFarm = SerializationHelper.FromString<AnimalFarm>(serializedStr));

			Assert.AreEqual(2, deserializedFarm.Animals.Count);
		}

		[Test]
		public void linq2xml_sample()
		{
			var contacts = GenerateContacts();

			var serializedStr = contacts.ToString();
			Assert.NotNull(serializedStr);
			Assert.IsFalse(string.IsNullOrWhiteSpace(serializedStr));
		}

		[Test]
		public void linq2xml_sample_from_string_casesensitive()
		{
			var contactsStr = GenerateContactsString();
			var xElem = XElement.Parse(contactsStr);

			var ret = xElem.Descendants("Phone")
				.Select(e => (string)e.Element("Phone")).ToList();

			Assert.AreEqual(2, ret.Count);
		}

		[Test]
		public void linq2xml_sample_query()
		{
			var contactsStr = GenerateContactsString();
			//XDocument.Load(stream).Root
			//var xElem = XElement.Parse(contactsStr);
			var xElem = XDocument.Parse(contactsStr).Root;

			var ret = xElem.Descendants("Phone")
				.Where(e => (string)e.Attributes("Type").Single() == "Home")
				.Select(e => (string)e.Element("Phone")).ToList();

			Assert.AreEqual(1, ret.Count);
		}

		[Test]
		public void parse_expando()
		{
			var contacts = new List<dynamic>();

			var doc = XDocument.Parse(GenerateContactsString());
			var nodes = doc.Descendants("Contacts").ToList();
			foreach (var n in nodes)
			{
				dynamic contact = new ExpandoObject();
				foreach (var child in n.Descendants())
				{
					var p = contact as IDictionary<string, object>;
					p[child.Name.ToString()] = child.Value.Trim();
				}
				contacts.Add(contact);
			}

			var c = contacts.First();
			var ct = new Contact()
			{
				Name = c.Name,
				Phone = string.IsNullOrWhiteSpace(c.phone) ? c.Phone : c.phone
			};

			Assert.AreEqual(1, contacts.Count);
		}

		class Contact
		{
			public string Name { get; set; }
			public string Phone { get; set; }
		}

		string GenerateContactsString()
		{
			return GenerateContacts().ToString();
		}
		XElement GenerateContacts()
		{
			return new XElement("Contacts",
					new XElement("Contact",
						new XElement("Name", "Patrick Hines"),
						new XElement("Phone", "206-555-0144",
							new XAttribute("Type", "Home")),
						new XElement("Phone", "425-555-0145",
							new XAttribute("Type", "Work")),
						new XElement("phone", "435-555-0145",
							new XAttribute("Type", "Work")),
						new XElement("Address",
							new XElement("Street1", "123 Main St"),
							new XElement("City", "Mercer Island"),
							new XElement("State", "WA"),
							new XElement("Postal", "68042")
						)
					)
				);
		}
	}
}