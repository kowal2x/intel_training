﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace IntelTraining.Serialization.Helpers
{
	public class AnimalFarm
	{
		//[XmlElement(typeof(Cat))]
		//[XmlElement(typeof(Fish))]
		public List<Animal> Animals { get; set; }

		public AnimalFarm()
		{
			Animals = new List<Animal>();
		}
	}
}