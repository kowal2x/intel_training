﻿using System.Xml.Serialization;

namespace IntelTraining.Serialization.Helpers
{
	[XmlInclude(typeof(Cat))]
	[XmlInclude(typeof(Fish))]
	public abstract class Animal
	{
		public int Weight { get; set; }
	}
}