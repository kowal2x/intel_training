using System;
using System.Collections.Generic;

namespace IntelTraining.Linq
{
	public class LambdaEqualityComparer<T, TValue> : IEqualityComparer<T>
	{
		private readonly Func<T, TValue> _extractKey;

		public LambdaEqualityComparer(Func<T, TValue> extractKey)
		{
			_extractKey = extractKey;
		}

		public bool Equals(T x, T y)
		{
			return GetHashCode(x) == GetHashCode(y);
		}

		public int GetHashCode(T obj)
		{
			return _extractKey(obj).GetHashCode();
		}
	}
}