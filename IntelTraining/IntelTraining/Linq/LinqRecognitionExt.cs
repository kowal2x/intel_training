﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace IntelTraining.Linq
{
	[TestFixture]
	public class LinqRecognitionExt
	{
		[Test]
		public void select_sample()
		{
			var students = GenerateStudents().Take(100);

			var onlyNames = students.Select(e => e.Name).ToList();
			var namesAndSurnames = students.Select(e => new { a = e.Name, b = e.Surname }).ToList();
			var people = students.Select(e => new Person() { Name = e.Name, Surname = e.IndexNo }).ToList();

			var people2 = (from Student e in students
						   select new Person() { Name = e.Name, Surname = e.IndexNo })
						.ToList();
		}

		[Test]
		public void where_sample()
		{
			var students = GenerateStudents().Take(100);

			var studentsWithHighAvarage = students.Where(e => e.Avarage > 4.5M).ToList();
			var studentsWithHighAvarageAndNoPayments =
				students.Where(e => e.Avarage > 4.5M && e.TotalPayments == 0M).ToList();
			var studentsWithHighAvarageAndNoPayments2 = students
				.Where(e => e.Avarage > 4.5M)
				.Where(e => e.TotalPayments == 0M).ToList();

			(from Student e in students
			 where e.Avarage > 4.5M
			 where e.TotalPayments == 0M
			 select e).ToList();
		}

		[Test]
		public void order_sample()
		{
			var students = GenerateStudents().Take(10000).ToList();

			var t1 = MeasureTime(() =>
				students
					.OrderBy(e => e.Surname)
					.ThenBy(e => e.Name)
					.Where(e => e.Avarage > 4.5M)
					.ToList());
			var t2 = MeasureTime(() =>
				students
					.Where(e => e.Avarage > 4.5M)
					.OrderBy(e => e.Surname)
					.ThenBy(e => e.Name)
					.ToList());

			Assert.Pass("time of execution: {0} {1}", t1, t2);
		}

		[Test]
		public void take_skip_sample()
		{
			var students = GenerateStudents().Take(5).ToList();

			var stud1 = students.Take(3).ToList();
			Assert.AreEqual(3, stud1.Count);

			var stud2 = students.Skip(1).Take(3).ToList();
			Assert.AreEqual(3, stud2.Count);
			Assert.AreSame(stud2.First(), stud1.Skip(1).First());
		}

		[Test]
		public void or_default()
		{
			var students = new List<Student>();

			Assert.Throws<InvalidOperationException>(() => students.First());
			Assert.IsNull(students.FirstOrDefault());

			Assert.Throws<InvalidOperationException>(() => students.Single());
			Assert.IsNull(students.SingleOrDefault());

			students.Add(new Student() { Avarage = 5.0M });
			Assert.Throws<InvalidOperationException>(() => students.Single(e => e.Avarage == 4.0M));
			Assert.IsNull(students.SingleOrDefault(e => e.Avarage == 4.0M));

			Assert.DoesNotThrow(() => students.Single(e => e.Avarage == 5.0M));
			Assert.NotNull(students.SingleOrDefault(e => e.Avarage == 5.0M));

			students.Add(new Student() { Avarage = 5.0M });

			Assert.DoesNotThrow(() => students.First(e => e.Avarage == 5.0M));
			Assert.NotNull(students.FirstOrDefault(e => e.Avarage == 5.0M));

			Assert.Throws<InvalidOperationException>(() => students.Single(e => e.Avarage == 5.0M));
			Assert.Throws<InvalidOperationException>(() => students.SingleOrDefault(e => e.Avarage == 5.0M));
		}

		[Test]
		public void count_any()
		{
			var students = GenerateStudents().Take(100000);

			var t1 = MeasureTime(() =>
			{
				var b = students.Count() > 0;
			});
			var t2 = MeasureTime(() => students.Any());

			Assert.GreaterOrEqual(t1, t2);
			Console.WriteLine("time of execution: {0} {1}", t1, t2);

			var materialized = students.ToList();
			var t3 = MeasureTime(() => materialized.Where(e => e.Avarage > 3).Any());
			var t4 = MeasureTime(() => materialized.Any(e => e.Avarage > 3));
			Assert.GreaterOrEqual(t3, t4);
			Console.WriteLine("time of execution: {0} {1}", t3, t4);
		}

		[Test]
		public void sequence_equal()
		{
			var nums1 = new List<int>() { 1, 2, 3, 4, 5 };
			var nums2 = new List<int>() { 1, 2, 3, 4, 5 };

			Assert.IsTrue(nums1.SequenceEqual(nums2));

			var studs1 = new List<Student>()
			{
				new Student(){Avarage = 2.0M, Name = "mb", Surname = "srn"},
				new Student(){Avarage = 3.0M, Name = "mb", Surname = "srn"}
			};
			var studs2 = new List<Student>()
			{
				new Student(){Avarage = 2.0M, Name = "mb", Surname = "srn"},
				new Student(){Avarage = 3.0M, Name = "mb", Surname = "srn"}
			};
			Assert.IsTrue(
				studs1.SequenceEqual(
					studs2,
					new StudentEqualityComparerByAvarage()));

			var lambdaComparer =
				new LambdaEqualityComparer<
					Student, Tuple<decimal, string, string>>(e => Tuple.Create(e.Avarage, e.Name, e.Surname));
			Assert.IsTrue(
				studs1.SequenceEqual(studs2, lambdaComparer));
		}

		[Test]
		public void set_operations()
		{
			var nums1 = new[] { 1, 2, 3 }.Union(new[] { 4, 5, 6 });
			Assert.AreEqual(6, nums1.Count());

			var nums2 = new[] { 1, 2, 3 }.Union(new[] { 3, 4, 5 });
			Assert.AreEqual(5, nums2.Count());

			var nums3 = new[] { 1, 2, 3 }.Concat(new[] { 3, 4, 5 });
			Assert.AreEqual(6, nums3.Count());

			var nums4 = new[] { 1, 1, 2, 3 }.Union(new[] { 2, 2, 4, 3 });
			Assert.AreEqual(4, nums4.Count());

			var nums5 = new[] { 1, 2, 3 }.Except(new[] { 3, 4, 5 });
			Assert.AreEqual(2, nums5.Count());

			var nums6 = new[] { 1, 1, 2, 2, 3, 4, 4, 4 }.Distinct();
			Assert.AreEqual(4, nums6.Count());

			var nums7 = new[] { 1, 2, 3 }.Intersect(new[] { 3, 4, 5 });
			Assert.AreEqual(1, nums7.Count());
		}

		[Test]
		public void group_sample()
		{
			var studs = new List<Student>()
			{
				new Student(){Avarage = 2.0M, Name = "mb", Surname = "srn"},
				new Student(){Avarage = 3.0M, Name = "mb1", Surname = "srn1"},
				new Student(){Avarage = 3.0M, Name = "mb2", Surname = "srn2"},
			}.ToList();

			var groupped = studs.GroupBy(e => e.Avarage).ToList();
			Assert.AreEqual(2, groupped.Count());
			Assert.AreEqual(2.0M, groupped.First().Key);
			Assert.AreEqual(3.0M, groupped.Skip(1).First().Key);
			Assert.AreEqual("mb", groupped.First().First().Name);
		}

		[Test]
		public void join_sample()
		{
			var infos = new List<AvarageInfo>()
			{
				new AvarageInfo(){Avarage = 2.0M, Description = "Mierny"},
				new AvarageInfo(){Avarage = 3.0M, Description = "Dostateczny"},
			};

			var studs = new List<Student>()
			{
				new Student(){Avarage = 2.0M, Name = "mb", Surname = "srn"},
				new Student(){Avarage = 3.0M, Name = "mb1", Surname = "srn1"},
				new Student(){Avarage = 3.0M, Name = "mb2", Surname = "srn2"},
			};

			var studentsWithAvarageDesc =
				studs.Join(infos, e => e.Avarage, e => e.Avarage,
					(student, info) => new
					{
						student.Name,
						student.Surname,
						AvarageDescription = info.Description
					})
				.ToList();

			Assert.AreEqual("Mierny", studentsWithAvarageDesc[0].AvarageDescription);
			Assert.AreEqual("Dostateczny", studentsWithAvarageDesc[1].AvarageDescription);
			Assert.AreEqual("Dostateczny", studentsWithAvarageDesc[2].AvarageDescription);
		}

		[Test]
		public void select_many()
		{
			var results = new List<SomerResults>()
			{
				new SomerResults() {Nums = new List<int>() {1, 2, 3}},
				new SomerResults() {Nums = new List<int>() {4, 5, 6}},
			};

			var allNumsWrong = results.Select(e => e.Nums).ToList();
			Assert.AreEqual(2, allNumsWrong.Count);

			//var allOthers = results.SelectMany(e => e.SomeVal).ToList();
			var allNums = results.SelectMany(e => e.Nums).ToList();
			Assert.AreEqual(6, allNums.Count);
		}

		#region helpers
		class SomerResults
		{
			public int SomeVal { get; set; }
			public List<int> Nums { get; set; }
		}

		class AvarageInfo
		{
			public decimal Avarage { get; set; }
			public string Description { get; set; }
		}
		class StudentEqualityComparerByAvarage : IEqualityComparer<Student>
		{
			public bool Equals(Student x, Student y)
			{
				return x.Avarage == y.Avarage;
			}

			public int GetHashCode(Student obj)
			{
				throw new NotImplementedException();
			}
		}

		IEnumerable<Student> GenerateStudents()
		{
			var rand = new Random();
			int i = 0;
			while (true)
			{
				yield return new Student()
				{
					Avarage = (decimal)rand.Next(100, 550) / 100,
					IndexNo = rand.Next(100000, 999999).ToString(),
					Name = "studentname" + i,
					Surname = "studentsurnname" + i,
					TotalPayments = rand.Next(0, 1000)
				};
			}
		}

		long MeasureTime(Action action)
		{
			var sw = new Stopwatch();
			sw.Start();
			action();
			sw.Stop();
			return sw.ElapsedMilliseconds;
		}
		#endregion
	}

	//without let - crap - many subqueries generated - hindu version
	//public IQueryable<Loan> ListLoansNotClosedOrNotRejected(int customerId)
	//{
	//	var list = Repository.GetAll().Where(l => l.CloseDate == null && l.CustomerId == customerId);

	//	list = list.Where(l => l.AccountP2P != null
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.Approved
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.WaitingForCreditCheck
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.OngoingCreditCheck
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.New
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.IncompleteCreditCheck
	//							|| l.LoanStatus.OrderByDescending(ls => ls.LoanStatusId).FirstOrDefault().LoanStatusType == LoanStatusType.ApprovedWaiting);
	//	return list;
	//}

	//let sample - better
	//public IQueryable<Loan> ListLoansNotClosedOrNotRejected(int customerId)
	//{
	//	var list = Repository.GetAll()
	//		.Where(l => l.CloseDate == null && l.CustomerId == customerId);

	//	return from itm in list
	//			let newestStatus = itm.LoanStatus
	//				.OrderByDescending(ls => ls.LoanStatusId).First().LoanStatusType
	//			where itm.AccountP2P != null
	//					|| newestStatus == LoanStatusType.Approved
	//					|| newestStatus == LoanStatusType.WaitingForCreditCheck
	//					|| newestStatus == LoanStatusType.OngoingCreditCheck
	//					|| newestStatus == LoanStatusType.New
	//					|| newestStatus == LoanStatusType.IncompleteCreditCheck
	//					|| newestStatus == LoanStatusType.ApprovedWaiting
	//			select itm;
	//}
}