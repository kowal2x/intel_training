﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	class BackendServiceDoSomethingRetryDecorator
	{
		private readonly IBackendService _backendService;

		private int _leftRetries;
		public int LeftRetries { get { return _leftRetries; } }

		public BackendServiceDoSomethingRetryDecorator(IBackendService backendService)
		{
			_backendService = backendService;
		}

		public OutputData DoSomething(InputData data)
		{
			_leftRetries = 3;
			OutputData ret = null;
			while ((--_leftRetries) > 0)
			{
				ret = _backendService.DoSomething(data);

				if (ret.Type != OutputType.Busy)
					break;
			}

			return ret;
		}
	}
}