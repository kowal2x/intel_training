﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	class ErrorStrategy : IBackendServiceDoSomethingResultDealingStrategy
	{
		private readonly IFrontentNotifier _frontentNotifier;

		public ErrorStrategy(IFrontentNotifier frontentNotifier)
		{
			_frontentNotifier = frontentNotifier;
		}

		public OutputType SuitableFor { get { return OutputType.Error; } }
		public void Deal(OutputData data)
		{
			_frontentNotifier.Notify("error: " + data.RetVal);
		}
	}
}