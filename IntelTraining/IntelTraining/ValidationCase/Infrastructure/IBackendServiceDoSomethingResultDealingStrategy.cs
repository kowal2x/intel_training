﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	interface IBackendServiceDoSomethingResultDealingStrategy
	{
		OutputType SuitableFor { get; }
		void Deal(OutputData data);
	}
}