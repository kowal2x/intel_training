﻿namespace IntelTraining.ValidationCase.Infrastructure
{
	public enum OutputType
	{
		Ok,
		Error,
		Busy
	}
}