using System.Windows.Forms;

namespace GameFoundation
{
	public interface IKeyboardStatus
	{
		bool IsKeyPressed(Keys key);
	}
}