﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GameFoundation
{
	public partial class GameForm : Form
	{
		private readonly IGame _game;

		public GameForm(IGame game)
		{
			InitializeComponent();

			_game = game;
			ClientSize = new Size(_game.Width, _game.Height);

			if (game.UsePerformanceOptimization)
				gameControl1.UsePerformanceOptimization = true;

			var dealer = _game.KeyboardStatus as IKeysHandler;
			if (dealer != null)
			{
				gameControl1.PreviewKeyDown += (s, e) => dealer.KeyDown(e.KeyCode);
				gameControl1.KeyUp += (s, e) => dealer.KeyUp(e.KeyCode);
			}
		}

		private void GameFormShown(object sender, EventArgs e)
		{
			Text = _game.Name;
			gameControl1.Render += (g, t) => _game.PerformNextIteration(g, t);
		}
	}
}