using System.Drawing;

namespace SomeGame.Infrastructure
{
	internal interface ISceneObject
	{
		double PositionX { get; }
		double PositionY { get; }

		void Draw(Graphics g);
		void Update(double deltaTime);
	}
}